@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<h1 style="font-size:33px;" class="lead">
  Fun Fact
</h1>
<b>following is fun facts about electric eel, The lying starts with their name.</b><br />
<b><br /></b>
<b>1. Electric eels aren’t actually eels. They’re members of the knife fish family.</b><br />
<b><br /></b>
<b>2. The critters are native to South American rivers, but they don’t spend all their time underwater. They have to come to the surface to breathe.</b><br />
<b><br /></b>
<b>3. All of an electric eel’s vital organs are crammed into the front 20 percent of its body. The rest is packed with 6000 cells that act like tiny batteries.</b><br />
<b><br /></b>
<b>4. With that much body devoted to electricity, it’s no wonder an eel can zap out more than 600 volts!</b><br />
<b><br /></b>
<b>5. An eel’s voltage is great for defense, but it also comes in handy for stunning prey, mostly crustaceans and small fish.</b><br />
<b><br /></b>
<b>6. Electric eels can’t see what they’re shocking. They’re mostly blind and use a radarlike system of electrical pulses to navigate and find food.</b><br />
<b><br /></b>
<b>7. Eels’ thick skin normally insulates them from their own attacks, but when wounded, they’ll shock themselves!</b><br />
<b><br /></b>
<b>8. A fully grown electric eel can be up to eight feet long and weigh 44 pounds!</b><br />
<b><br /></b>
<b>9. Fatal attacks on humans are rare, but that doesn’t mean eels are harmless. People have drowned after being shocked.</b><br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-_EAmCtjk5mw/Vt5LPBKbFLI/AAAAAAAAADU/WDp2vJldzH8/s1600/images%2B%25283%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://3.bp.blogspot.com/-_EAmCtjk5mw/Vt5LPBKbFLI/AAAAAAAAADU/WDp2vJldzH8/s1600/images%2B%25283%2529.jpg" /></a></div>
<b><br /></b>
<b>10. Their long and cylindrical body can be white, black, blue, purple or grey in color.</b><br />
<b><br /></b>
<b>11. Electric eels are solitary animals (live on their own). Group of eels is called swarm.</b><br />
<b><br /></b>
<b>12. Electric eel can produce electric shock strong enough to knock down a horse.</b><br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-1ya80FNPzoo/Vt5LPM900YI/AAAAAAAAADg/r_j-2sDElwU/s1600/unduhan%2B%25284%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-1ya80FNPzoo/Vt5LPM900YI/AAAAAAAAADg/r_j-2sDElwU/s1600/unduhan%2B%25284%2529.jpg" /></a></div>
<b><br /></b>
<b>13.20 foot long eel can produce enough electricity to light 12 light bulbs.</b><br />
<b><br /></b>
<b>14.Since eels have poor eyesight, they generate low-level electric charge (up to 10 volts) that helps them see their surrounding and locate a prey.</b><br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-mAUEBLLlljo/Vt5LPHaoDaI/AAAAAAAAADY/zWIK76FfNj0/s1600/images%2B%25284%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="138" src="https://3.bp.blogspot.com/-mAUEBLLlljo/Vt5LPHaoDaI/AAAAAAAAADY/zWIK76FfNj0/s320/images%2B%25284%2529.jpg" width="320" /></a></div>
<b><br /></b>
<b>15. Newly hatched eels eat small invertebrates but they also search nests of other eels and steal their eggs.</b><br />
<b><br /></b>
<b>16. Electric eel lives around 15 years in the wild and up to 22 years in captivity.</b><br />
<div>
<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-K6Q5Y3DO93k/Vt5LQFhcJBI/AAAAAAAAADc/nodLbm4GBKU/s1600/unduhan%2B%25285%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://1.bp.blogspot.com/-K6Q5Y3DO93k/Vt5LQFhcJBI/AAAAAAAAADc/nodLbm4GBKU/s1600/unduhan%2B%25285%2529.jpg" /></a></div>
<br /></div>
<ul style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
</ul>
</div>


@stop
