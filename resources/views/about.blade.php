@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<h1 style="font-size:33px;" class="lead">
  About Us
</h1>
About Electric Eel Team<br />
<br />
- Ali Reza (ali.reza@tokopedia.com)<br />
- Diny Rahmanto (diny.rahmanto@tokopedia.com)<br />
- Ratu Milagita (ratu.milagita@tokopedia.com)<br />
- Yusnita Desmawari (yusnita.desmawari@tokopedia.com)<br />
- Andika Seto (andika.seto@tokopedia.com)<br />
- Jean Karunadewi (jean.karunadewi@tokopedia.com)<br />
<br />
<a href="{{ asset('contact') }}"> Contact Us !</a>

<br><br>
<div class="" style="background:#efe;">
  This blog is developed by Electric EEL team by using <a href="https://www.scrum.org/">Scrum Methods</a>
<br />
<br />
<div style="color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<b>Scrum</b>&nbsp;is an iterative and incremental&nbsp;<a href="https://en.wikipedia.org/wiki/Agile_software_development" style="background: none; color: #0b0080; text-decoration: none;" title="Agile software development">agile software development</a>&nbsp;framework for managing product development.&nbsp;It defines "a flexible,&nbsp;<a href="https://en.wikipedia.org/wiki/Holism" style="background: none; color: #0b0080; text-decoration: none;" title="Holism">holistic</a>&nbsp;product development strategy where a development team works as a unit to reach a common goal",&nbsp;challenges assumptions of the "traditional, sequential approach"&nbsp;to product development, and enables teams to self-organize by encouraging physical co-location or close online collaboration of all team members, as well as daily face-to-face communication among all team members and disciplines in the project.</div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
A key principle of scrum is its recognition that during production processes, the customers can change their minds about what they want and need (often called requirements volatility), and that unpredicted challenges cannot be easily addressed in a traditional predictive or planned manner. As such, scrum adopts an&nbsp;<a class="mw-redirect" href="https://en.wikipedia.org/wiki/Empirical" style="background: none; color: #0b0080; text-decoration: none;" title="Empirical">empirical</a>&nbsp;approach—accepting that the problem cannot be fully understood or defined, focusing instead on maximizing the team's ability to deliver quickly, to respond to emerging requirements and to adapt to evolving technologies and changes in market conditions.</div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
How we use scrum</div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<b>Workflow of Scrum</b></div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<b>Planning</b></div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
At the beginning of a sprint, the team holds a&nbsp;<i>sprint planning event</i>&nbsp;that:</div>
<ul style="color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">Communicates the scope of work likely during that sprint</li>
<li style="margin-bottom: 0.1em;">Selects product backlog items that likely can be done</li>
<li style="margin-bottom: 0.1em;">Prepares the sprint backlog that details the work needed to finish the selected product backlog items, with the entire team</li>
<li style="margin-bottom: 0.1em;">Sets a four-hour time planning event limit for a two-week sprint (pro rata for other sprint durations)&nbsp;<sup class="reference" id="cite_ref-scrumguide_15-1" style="font-size: 11.2px; line-height: 1; unicode-bidi: isolate;"><a href="https://en.wikipedia.org/wiki/Scrum_(software_development)#cite_note-scrumguide-15" style="background: none; color: #0b0080; text-decoration: none; white-space: nowrap;">[15]</a></sup><ul style="list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); list-style-type: disc; margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">During the first half, the whole team (development team, scrum master, and product owner) agree what product backlog items to consider for that sprint</li>
<li style="margin-bottom: 0.1em;">During the second half, the development team decomposes the work (tasks) required to deliver those backlog items, resulting in the&nbsp;<i>sprint backlog</i></li>
</ul>
</li>
</ul>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
Once the development team prepares the&nbsp;<i>sprint backlog</i>, they commit (usually by voting) to deliver tasks within the sprint.</div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<b>Sprint</b></div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
Each day during a sprint, the team holds a&nbsp;<i>daily scrum</i>&nbsp;(or&nbsp;<a href="https://en.wikipedia.org/wiki/Stand-up_meeting" style="background: none; color: #0b0080; text-decoration: none;" title="Stand-up meeting"><i>stand-up</i></a>) with specific guidelines:</div>
<ul style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">All members of the development team come prepared. The daily scrum...<ul style="list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); list-style-type: disc; margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">...starts precisely on time even if some development team members are missing</li>
<li style="margin-bottom: 0.1em;">...should happen at the same time and place every day</li>
<li style="margin-bottom: 0.1em;">...is limited (<a href="https://en.wikipedia.org/wiki/Timeboxing" style="background: none; color: #0b0080; text-decoration: none;" title="Timeboxing">timeboxed</a>) to fifteen minutes</li>
</ul>
</li>
<li style="margin-bottom: 0.1em;">Anyone is welcome, though normally only scrum team roles contribute.</li>
<li style="margin-bottom: 0.1em;">During the daily scrum, each team-member answers three questions:<ul style="list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); list-style-type: disc; margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">What did I do yesterday that helped the development team meet the sprint goal?</li>
<li style="margin-bottom: 0.1em;">What will I do today to help the development team meet the sprint goal?</li>
<li style="margin-bottom: 0.1em;">Do I see any impediment that prevents me or the development team from meeting the sprint goal?</li>
</ul>
</li>
</ul>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
Any impediment (stumbling block, risk or issue) identified in the daily scrum should be captured by the scrum master and displayed on the team's scrum board, with an agreed person designated to working toward a resolution (outside of the daily scrum). No detailed discussions should happen during the daily scrum.</div>
<div style="color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<span style="line-height: 22.4px;"><b>Review</b></span></div>
<div style=" color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
At the end of a sprint, the team holds two events: the&nbsp;<i>sprint review</i>&nbsp;and the&nbsp;<i>sprint retrospective</i>.</div>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
At the&nbsp;<b>sprint review</b>, the team:</div>
<ul style="color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">Reviews the work that was completed and the planned work that was not completed</li>
<li style="margin-bottom: 0.1em;">Presents the completed work to the stakeholders (a.k.a. the&nbsp;<i>demo</i>)</li>
</ul>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
Guidelines for sprint reviews:</div>
<ul style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">Incomplete work cannot be demonstrated</li>
<li style="margin-bottom: 0.1em;">The recommended duration is two hours for a two-week sprint (pro-rata for other sprint durations)</li>
</ul>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
<span style="line-height: 22.4px;"><b>Retrospective</b></span></div>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
At the&nbsp;<b>sprint retrospective</b>, the team:</div>
<ul style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">Reflects on the past sprint</li>
<li style="margin-bottom: 0.1em;">Identifies and agrees on continuous process improvement actions</li>
</ul>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
Guidelines for sprint retrospectives:</div>
<ul style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
<li style="margin-bottom: 0.1em;">Two main questions are asked in the sprint retrospective: What went well during the sprint? What could be improved in the next sprint?</li>
<li style="margin-bottom: 0.1em;">The recommended duration is one-and-a-half hours for a two-week sprint (pro-rata for other sprint durations)</li>
<li style="margin-bottom: 0.1em;">This event is facilitated by the scrum master</li>
<li style="margin-bottom: 0.1em;">Backlog refinement</li>
</ul>
<div style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; margin-bottom: 0.5em; margin-top: 0.5em;">
The workflow enable the team to make a clear division of task, working in quick, flexible, collaborative, and high speed environment.</div>
</div>
</div>
@stop
