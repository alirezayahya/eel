<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scrum Zoo - Electric Eel</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/clean-blog.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style media="screen">
      html,body{
        background:#fff;
        font-family: "Times New Roman" !important;
        color:black;

      }
      html,body, a, p, .lead, h1, h2, h3, h4, h5, h6, span{
          font-family: "Times New Roman" !important;
      }
      a{
        color:blue !important;
        text-decoration: underline !important;
      }
    </style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top" style="background:rgba(0,0,0,0.5)">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ asset('/') }}">
                  <img src="{{ asset('img/logo-green.png') }}" style="float:left;margin-right:20px;" height="25" alt="" />
                  <small style="font-weight:100;">
                  Electric Eel
                </small>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ asset('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ asset('/behaviour') }}">Behaviour</a>
                    </li>
                    <li>
                        <a href="{{ asset('/about') }}">About</a>
                    </li>
                    <li>
                        <a href="{{ asset('/habitat') }}">Habitat</a>
                    </li>
                    <li>
                        <a href="{{ asset('/classification') }}">Classification</a>
                    </li>
                    <li>
                        <a href="{{ asset('/communication') }}">Communication</a>
                    </li>
                    <li>
                        <a href="{{ asset('/gallery') }}">Gallery</a>
                    </li>
                    <li>
                        <a href="{{ asset('/pop-culture') }}">Popular Culture</a>
                    </li>
                    <li>
                        <a href="{{ asset('/reproduction') }}">Reproduction</a>
                    </li>
                    <li>
                        <a href="{{ asset('/fun-fact') }}">Fun Fact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    @yield('content')

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('js/clean-blog.min.js') }}"></script>


    <footer class="text-center" style="font-size:11px;">
      <a href="{{ asset('/') }}">Home</a> -
      <a href="{{ asset('/about') }}">About</a> -
      <a href="http://thescrumzoo8.blogspot.co.id/p/contact-us.html">Contact Us</a> |

     Powered By Scrum Zoo
    </footer>

    </body>

    </html>
