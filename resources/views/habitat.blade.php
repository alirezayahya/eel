@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<div class="lead">
  Habitat
</div>


<img src="{{ asset('img/gallery/swamp-eel.jpeg') }}" style="width:100%;" alt="" />

<p>Although they are called eels, they are more closely related to the catfish than to the common eels.</p>

<p>&nbsp;</p>

<p>Electric eels inhabit fresh waters of the Amazon and Orinoco River basins in South America, in floodplains, swamps, creeks, small rivers, and coastal plains.</p>

<p>&nbsp;</p>

<p>They often live on muddy bottoms in calm or stagnant waters.</p>

<p>&nbsp;</p>

<p>They live in shallow, muddy water and come to the surface every 10 minutes because they breathe atmospheric air.</p>


</div>


@stop
