<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('{{ asset('img/youve-heard-of-electric-eels-her.jpg') }}')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h2>Scrum Zoo - Electric Eel</h2>
                    <hr class="small">
                    <span class="subheading">Agile. Flexible. Durable.</span>
                </div>
            </div>
        </div>
    </div>
</header>
