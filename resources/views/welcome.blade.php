@extends('_layout')


@section('content')
 <style media="screen">

   input{
     height:60px;
     font-size:22px;
     width:100%;
   }
   .gsc-control{
     width:790px !important;
   }
 </style>
  @include('header')

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-preview">
                    <a>
                        <h2 class="post-title text-center">
                            Search
                        </h2>
                        <h3 class="post-subtitle">
                          {{--
                          <form class=""  method="get">


                            <input type="text" class="form-control text-center" onkeyup="search(this.value)" placeholder="Search Your Favorite Electric Eel" style="height:60px;font-size:22px;" name="keyword" value="">
                            </form>
                            --}}
                        </h3>
                    </a>

                </div>
                <hr>
                <div id="searchcontrol" style="width:100%;">
                </div>

                <!-- Pager -->

            </div>
        </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://www.google.com/jsapi"
        type="text/javascript"></script>
    <script language="Javascript" type="text/javascript">
    //<!
    var searchControl = null;
    google.load('search', '1');
    var keywords = "Eel";
    function OnLoad() {
      // Create a search control
      searchControl = new google.search.SearchControl();

      // Add in a full set of searchers
      var localSearch = new google.search.LocalSearch();
      searchControl.addSearcher(new google.search.WebSearch());
      searchControl.addSearcher(new google.search.VideoSearch());
      searchControl.addSearcher(new google.search.BlogSearch());
      searchControl.addSearcher(new google.search.NewsSearch());
      searchControl.addSearcher(new google.search.ImageSearch());
      searchControl.addSearcher(new google.search.BookSearch());
      searchControl.addSearcher(new google.search.PatentSearch());

      // Set the Local Search center point
      localSearch.setCenterPoint("Indonesia, ID");

      // tell the searcher to draw itself and tell it where to attach
      searchControl.draw(document.getElementById("searchcontrol"));

      // execute an inital search
      searchControl.execute(keywords);
      $('input').attr('placeholder', 'Search Term')
    }
    google.setOnLoadCallback(OnLoad);
    function search(keywords){
      searchControl.execute(keywords);

    }
    //]]>
    </script>
@stop
