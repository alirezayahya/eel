@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<div class="lead">
  Gallery
</div>


  @foreach($pictures as  $key => $files)
  <div class="row">
    @foreach($files as $file)


    <div class="col-xs-6 col-md-3">
      <a href="{{ asset('img/gallery/'. $file) }}" target="_blank" class="thumbnail">
        <img src="{{ asset('img/gallery/'. $file) }}" alt="...">
      </a>
      <small>{{ beautify($file) }}</small>
    </div>
    @endforeach
    </div>
  @endforeach
  <div class="row">
    <div class="col-xs-6 col-md-3">
      <a href="https://3.bp.blogspot.com/-2D4ANzd0QH0/Vt5N7zE9zFI/AAAAAAAAAD8/DXolef6p-_I/s1600/animals-animal-electric_eels-eels-electrons-electricity-cgan2464_low.jpg" target="_blank" class="thumbnail">
        <img src="https://3.bp.blogspot.com/-2D4ANzd0QH0/Vt5N7zE9zFI/AAAAAAAAAD8/DXolef6p-_I/s1600/animals-animal-electric_eels-eels-electrons-electricity-cgan2464_low.jpg" alt="...">
      </a>

    </div>
    <div class="col-xs-6 col-md-3">
      <a href="https://3.bp.blogspot.com/-utskokTcV2g/Vt5NWLeCqQI/AAAAAAAAAD4/wCvY_MrRQ0s/s1600/rq_yin_and_the_electric_eels_by_fairyfookryskitty-d6x3z3k.jpg" target="_blank" class="thumbnail">
        <img src="https://3.bp.blogspot.com/-utskokTcV2g/Vt5NWLeCqQI/AAAAAAAAAD4/wCvY_MrRQ0s/s1600/rq_yin_and_the_electric_eels_by_fairyfookryskitty-d6x3z3k.jpg" alt="...">
      </a>

    </div>
    <div class="col-xs-6 col-md-3">
      <a href="https://4.bp.blogspot.com/-VfSNCfhfd8E/Vt5NVYqgfXI/AAAAAAAAADw/55FcPFawE-s/s1600/images%2B%25285%2529.jpg" target="_blank" class="thumbnail">
        <img src="https://4.bp.blogspot.com/-VfSNCfhfd8E/Vt5NVYqgfXI/AAAAAAAAADw/55FcPFawE-s/s1600/images%2B%25285%2529.jpg" alt="...">
      </a>

    </div>

    <div class="col-xs-6 col-md-3">
      <a href="https://3.bp.blogspot.com/-wyNOVJ6YOyc/Vt5N76UsL-I/AAAAAAAAAEA/4Ge3kZ53xZU/s1600/images%2B%25287%2529.jpg" target="_blank" class="thumbnail">
        <img src="https://3.bp.blogspot.com/-wyNOVJ6YOyc/Vt5N76UsL-I/AAAAAAAAAEA/4Ge3kZ53xZU/s1600/images%2B%25287%2529.jpg" alt="...">
      </a>

    </div>

  </div>


  <hr>
<form enctype="multipart/form-data" method="post">
  <input type="file" name="image">
  <button type="submit" class="btn btn-primary">Upload Your Own Picture !</button>
</form>



</div>


@stop
