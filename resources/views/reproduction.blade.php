@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<div class="lead">
  Reproduction
</div>
<p>Male electric eels tend to live up to around fifteen years (when in captive) but, females live from about 12 - 22 years, and at their adult size are about 2.5 meters or 8 feet.&nbsp;</p>

<p>Electric eels reproduction system is very unique! First the male partner creates a concealed nest using his SALIVA!, Then when the nest is finished, the female partner places her eggs inside.</p>

<p>The male closely watches the over the eggs until mid-January. 1,200 - 17,000 eels are born to a single nest.&nbsp;</p>

<p>Most newborn eels feed on invertebrate near and on the riverbed. Some have even eaten eggs from nests of that were laid shortly after their own.</p>

<p>a time period in which there is little rain in the Tropics. The time it takes for an Electric Eel to develop (AKA, The gestation period) is 6-7 months.&nbsp;</p>

<p>Both parents protect and care for their young.</p>

<p>&nbsp;</p>

<p>Electric eel egg</p>

<p>Now for the life cycle! The electric eel starts life as an egg and from there grows into an adult. When they first hatch they become larvae.&nbsp;</p>

<p>After larvae they are known as &quot; Glass Eels&quot;. After the Glass eel stage, they are called Elver. By the time they are Elver they are likely to be mature, most Electric Eels tend to mature around the ages of 10- 14 month.</p>

<p>The Elver stage is the last stage before the adult stage, and once they are adults they are able to reproduce. Electric Eels lay their eggs during a time period known as the dry season.</p>

<p>&nbsp;</p>

<img src="{{ asset('img/gallery/mighty-and-colorful-eel.jpg') }}" width="100%" alt="" />

</div>


@stop
