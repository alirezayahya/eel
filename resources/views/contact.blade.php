@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<h1 style="font-size:33px;" class="lead">
  Contact
</h1>

<form class="" method="post" class="col-md-4 col-md-offset-4">
  <input type="email" class="form-control" name="email" placeholder="Email" value="">
  <input type="text" class="form-control" name="subject" placeholder="Subject" value="">
  <textarea name="message" class="form-control" rows="8" placeholder="Message" cols="40"></textarea>
  <button type="submit" name="button" class="btn btn-primary">Send</button>
</form>
</div>


@stop
