@extends('_layout')


@section('content')
<div class="" style="min-height: 100px;">

</div>
<div class="container">
<h1 style="font-size:33px;" class="lead">
  Popular Culture
</h1>
<b>The Myth</b><br />
<br />
Although it looks like an eel, Electrophorus Electricus is actually a Knife Fish that is able to generate and deliver significant electric shocks of up to 600 volts.<br />
The ancient Egyptians referred to an electric eel as the “Thunderer of the Nile” which indicates that they had already made the connection to storm-related atmospheric discharges – lightning. According to various sources the Greeks and Romans were familiar with these creatures and may well have bred them in captivity. Historic records show that they were certainly farming many other types of exotic fish both for food and for amusement.<br />
<br />
Scribonus Largus, a physician at the court of the Roman Emperor Claudius (c.47AD), is reported to have written that these ‘torpedo fish’ could be used to treat a wide variety of ailments. They were used to numb the feet of gout sufferers as well as those suffering from persistent headaches. If this is true then this is the first recorded use of shock therapy. As recently as 2009 doctors in Boston have been successfully experimenting with electric currents to block migraines.<br />
The Knife Fish of South America is capable of delivering between 500 – 600 volts of electricity. The Nile (Electric) Catfish, Malapterurus Electricus, is capable of delivering approximately 350 volts. Photo credit: Wikimedia Commons: Steinhart Aquarium – Photographer: Stan Shebs – 2005.<br />
http://www.aquiziam.com/mysteries/mysterious-history/ancient-electricity/<br />
<br />
<b>Real Documentary about Electric Eel</b><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<iframe allowfullscreen="" class="YOUTUBE-iframe-video" data-thumbnail-src="https://i.ytimg.com/vi/5oRdXvM6CDY/0.jpg" frameborder="0" height="266" src="https://www.youtube.com/embed/5oRdXvM6CDY?feature=player_embedded" width="320"></iframe></div>
<br />
<div>
<b>Movie</b><br />
<b><br /></b> <span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">Otto and the Electric Eel (2011)&nbsp;</span><br />
<span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">Short Movie, Comedy, Horror</span><span class="para_break" style="background-color: #f9f9f9; box-sizing: border-box; color: #2c2d30; display: block; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; height: 0.5rem; line-height: 22px;"><i class="copy_only" style="-webkit-user-select: none; background-repeat: no-repeat; background-size: 0px; box-sizing: border-box; color: transparent; display: inline-block; float: left; font-size: 0px; height: 0px; text-rendering: auto; vertical-align: baseline;"><br style="box-sizing: border-box;" /></i></span><span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">Deep Shock (2003)</span><br />
<span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">Science Fiction, Horror</span><span class="para_break" style="background-color: #f9f9f9; box-sizing: border-box; color: #2c2d30; display: block; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; height: 0.5rem; line-height: 22px;"><i class="copy_only" style="-webkit-user-select: none; background-repeat: no-repeat; background-size: 0px; box-sizing: border-box; color: transparent; display: inline-block; float: left; font-size: 0px; height: 0px; text-rendering: auto; vertical-align: baseline;"><br style="box-sizing: border-box;" /></i></span><span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">The Little Mermaid (1989)</span><br />
<span style="background-color: #f9f9f9; color: #2c2d30; font-family: , &quot;applelogo&quot; , sans-serif; font-size: 15px; line-height: 22px;">Animated Film</span></div>
<hr>

<b>ANIMAL CARTOON ABOUT ELECTRIC EEL</b><br />
<b><br /></b>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-yf4UwyVwrKE/Vt5NVauAczI/AAAAAAAAADs/yi-bOSd7Y_Y/s1600/animal-kingdom-play-playing-eels-electric_eels-fish-08332490_low.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="320" src="https://3.bp.blogspot.com/-yf4UwyVwrKE/Vt5NVauAczI/AAAAAAAAADs/yi-bOSd7Y_Y/s320/animal-kingdom-play-playing-eels-electric_eels-fish-08332490_low.jpg" width="288" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-VfSNCfhfd8E/Vt5NVYqgfXI/AAAAAAAAADw/55FcPFawE-s/s1600/images%2B%25285%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-VfSNCfhfd8E/Vt5NVYqgfXI/AAAAAAAAADw/55FcPFawE-s/s1600/images%2B%25285%2529.jpg" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-NQjoqUNIBsg/Vt5NVb96S8I/AAAAAAAAAD0/JV3mFNDRzfw/s1600/images%2B%25286%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-NQjoqUNIBsg/Vt5NVb96S8I/AAAAAAAAAD0/JV3mFNDRzfw/s1600/images%2B%25286%2529.jpg" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-utskokTcV2g/Vt5NWLeCqQI/AAAAAAAAAD4/wCvY_MrRQ0s/s1600/rq_yin_and_the_electric_eels_by_fairyfookryskitty-d6x3z3k.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="213" src="https://3.bp.blogspot.com/-utskokTcV2g/Vt5NWLeCqQI/AAAAAAAAAD4/wCvY_MrRQ0s/s320/rq_yin_and_the_electric_eels_by_fairyfookryskitty-d6x3z3k.jpg" width="320" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-2D4ANzd0QH0/Vt5N7zE9zFI/AAAAAAAAAD8/DXolef6p-_I/s1600/animals-animal-electric_eels-eels-electrons-electricity-cgan2464_low.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="320" src="https://3.bp.blogspot.com/-2D4ANzd0QH0/Vt5N7zE9zFI/AAAAAAAAAD8/DXolef6p-_I/s320/animals-animal-electric_eels-eels-electrons-electricity-cgan2464_low.jpg" width="305" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-wyNOVJ6YOyc/Vt5N76UsL-I/AAAAAAAAAEA/4Ge3kZ53xZU/s1600/images%2B%25287%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://3.bp.blogspot.com/-wyNOVJ6YOyc/Vt5N76UsL-I/AAAAAAAAAEA/4Ge3kZ53xZU/s1600/images%2B%25287%2529.jpg" /></a></div>
<b><br /></b>
<ul style="background-color: white; color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px; list-style-image: url(&quot;data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%225%22%20height%3D%2213%22%3E%0A%3Ccircle%20cx%3D%222.5%22%20cy%3D%229.5%22%20r%3D%222.5%22%20fill%3D%22%2300528c%22%2F%3E%0A%3C%2Fsvg%3E%0A&quot;); margin: 0.3em 0px 0px 1.6em; padding: 0px;">
</ul>

</div>


@stop
