<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

function beautify($file){
$word = explode('.',$file)[0];
$word = str_replace('-',' ',  $word);
return ucwords($word);
}

Route::get('/', function () {
    return view('welcome');
});

Route::get('/anatomy', function () {
    return view('anatomy');
});

Route::get('/fun-fact', function () {
    return view('fun-fact');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/pop-culture', function () {
    return view('pop-culture');
});

Route::get('/communication', function () {
    return view('communication');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::post('/contact', 'UploadController@sendMail');

Route::get('/behaviour', function () {
    return view('behaviour');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/habitat', function () {
    return view('habitat');
});

Route::get('/classification', function () {
    return view('classification');
});

Route::get('/gallery', function () {
  $pictures = scandir('./img/gallery');
  unset($pictures[0],$pictures[1]);
  $pictures = array_chunk($pictures, 4);
    return view('gallery')
              ->with('pictures', $pictures);
});

Route::post('/gallery', 'UploadController@upload');

Route::get('/reproduction', function () {
    return view('reproduction');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
