<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UploadController extends Controller
{
    public function upload(Request $request){
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $destinationPath = './img/gallery';
        $fileName = $image->getClientOriginalName();
        $image->move($destinationPath, $fileName);
      }
      return redirect('gallery');
    }

    public function sendMail(Request $request){

      $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('contact')
                        ->withErrors($validator)
                        ->withInput();
        }

        

      return redirect('contact');
    }
}
